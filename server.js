var express = require('express');
var bodyParser = require('body-parser');

var win = [
[1, 1, 1, 1, 1],
[-1,-1,-1,-1,-1]
];
var unCovered4 = [
[0, 1, 1, 1, 1, 0],
[0, -1, -1, -1, -1, 0]
];
var unCovered3 = [
[0, 1, 1, 1, 0, 0],
[0, 0, 1, 1, 1, 0],
[0, 1, 0, 1, 1, 0],
[0, 1, 1, 0, 1, 0],
[0, -1, -1, -1, 0, 0],
[0, 0, -1, -1, -1, 0],
[0, -1, 0, -1, -1, 0],
[0, -1, -1, 0, -1, 0]
];
var unCovered2 = [
[0, 0, 1, 1, 0, 0],
[0, 1, 0, 1, 0, 0],
[0, 0, 1, 0, 1, 0],
[0, 1, 1, 0, 0, 0],
[0, 0, 0, 1, 1, 0],
[0, 1, 0, 0, 1, 0],
[0, 0, -1, -1, 0, 0],
[0, -1, 0, -1, 0, 0],
[0, 0, -1, 0, -1, 0],
[0, -1, -1, 0, 0, 0],
[0, 0, 0, -1, -1, 0],
[0, -1, 0, 0, -1, 0]
];
var covered4 = [
[-1, 1, 0, 1, 1, 1],
[-1, 1, 1, 0, 1, 1],
[-1, 1, 1, 1, 0, 1],
[-1, 1, 1, 1, 1, 0],
[0, 1, 1, 1, 1, -1],
[1, 0, 1, 1, 1, -1],
[1, 1, 0, 1, 1, -1],
[1, 1, 1, 0, 1, -1],
[1, -1, 0, -1, -1, -1],
[1, -1, -1, 0, -1, -1],
[1, -1, -1, -1, 0, -1],
[1, -1, -1, -1, -1, 0],
[0, -1, -1, -1, -1, 1],
[-1, 0, -1, -1, -1, 1],
[-1, -1, 0, -1, -1, 1],
[-1, -1, -1, 0, -1, 1]
];
var covered3 = [
[-1, 1, 1, 1, 0, 0],
[-1, 1, 1, 0, 1, 0],
[-1, 1, 0, 1, 1, 0],
[0, 0, 1, 1, 1, -1],
[0, 1, 0, 1, 1, -1],
[0, 1, 1, 0, 1, -1],
[-1, 1, 0, 1, 0, 1, -1],
[-1, 0, 1, 1, 1, 0, -1],
[-1, 1, 1, 0, 0, 1, -1],
[-1, 1, 0, 0, 1, 1, -1],
[1, -1, -1, -1, 0, 0],
[1, -1, -1, 0, -1, 0],
[1, -1, 0, -1, -1, 0],
[0, 0, -1, -1, -1, 1],
[0, -1, 0, -1, -1, 1],
[0, -1, -1, 0, -1, 1],
[1, -1, 0, -1, 0, -1, 1],
[1, 0, -1, -1, -1, 0, 1],
[1, -1, -1, 0, 0, -1, 1],
[1, -1, 0, 0, -1, -1, 1]
];

var coveredME = [
[-1, 1, 0, 0],
[0, 0, 1, -1],
[0, 1, 0, -1],
[-1, 0, 1, 0],
[1, -1, 0, 0],
[0, 0, -1, 1],
[0, -1, 0, 1],
[1, 0, -1, 0]
];

var coveredYOU = [
[1, -1, -1, 0],
[0, -1, -1, 1],
[-1, 1, 1, 0],
[0, 1, 1, -1]
];



Array.matrix = function(m, n, initial) {
var a, i, j, mat = [];
for (i = 0; i < m; i++) {
a = [];
for (j = 0; j < n; j++) {
  a[j] = initial;
}
mat[i] = a;
}
return mat;
};


var gameSize = 5; //5 pions en ligne pour gagner
var ring = 1; //Cercle autour de la position actuel ? propagation ?
//var win = false; 
var cellsCount = 19; // 19 cellules par lignes
var curState = Array.matrix(cellsCount, cellsCount, 0); //matrice de 19*19
var complexity = 1; // complexité du jeu 
var maxPlayer = 1; // X = 1, O = -1


var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use(function (request, response, next) {
  response.header("Access-Control-Allow-Origin",  "*");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, token, Accept");
    response.header("Access-Control-Allow-Methods", "PUT");
  next();
});
var compteurTenaille = 0;
app.put('/board', function (req, res) {
    
    var current_board = req.body.board;
    var me = req.body.player
    var compteurTenaille = req.body.score;
    var compteurVS = req.body.score_vs;
    var round = req.body.round;
    for (var i = 0; i < current_board.length; i++) {
    	for (var j = 0; j < current_board.length; j++) {
     	 if(current_board[i][j]==2){
      	  current_board[i][j]=-1;
     	 }
   	 }
    }
    if(me==2){
    	for (var i = 0; i < current_board.length; i++) {
			 for (var j = 0; j < current_board.length; j++) {
			 	if(current_board[i][j]==-1){
      	 		 current_board[i][j]=1;
     			 }
    		}
    	} 
    }
    for (var i = 0; i < current_board.length; i++) {
      for (var j = 0; j < current_board.length; j++) {
        if(current_board[i][j]!=curState[i][j]){
          var x = i;
          var y = j;
        }
      }
    }
    if (round == 3){
      do {
        var X = Math.floor(Math.random() * 17 + 1);
      } while (!(X in [6,7,8,9,10]));
      do {
        var Y = Math.floor(Math.random() * 17 + 1);
      } while (!(Y in [6,7,8,9,10]));
      var response= '{ "x":'+X+',"y":'+Y+'}';
    }
    else {
      console.log('x, y :' + x +y);
      var response = main(x,y);
    }
    /*if(player==1&&round==3){
      var X = Math.floor(Math.random() * 17 + 1);
      if(X in [8,9,10])return blabla
    }*/

  var obj = JSON.parse(response);

    res.send(obj);
});

var server = app.listen(8012, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)
});


//Position possible en dur



//Calcul du score
var valueCombo = function(w, u2, u3, u4, c3, c4,cme, cyou) {
if (w > 0) return 1000000000;
if (u4 > 0) return 100000000;
if (c4 > 1) return 10000000;
if (u3 > 0 && c4 > 0) return 1000000;
if (u3 > 1) return 100000;
//if (compteurTenaille == 4 && cyou > 0) return 100000000;

if (u3 == 1) {
  if (u2 == 3) return 40000;
  if (u2 == 2) return 38000;
  if (u2 == 1) return 35000;
  return 3450;
}

if (c4 == 1) {
  if (u2 == 3) return 4500;
  if (u2 == 2) return 4200;
  if (u2 == 1) return 4100;
  return 4050;
}
//if(compteurTenaille == 3 && cyou > 0) return 4000;

if (c3 == 1) {
  if (u2 == 3) return 3400;
  if (u2 == 2) return 3300;
  if (u2 == 1) return 3100;
}

//if(coveredYOU >= 1) return 3050;


if (c3 == 2) {
  if (u2 == 2) return 3000;
  if (u2 == 1) return 2900;
}

if (c3 == 3) {
  if (u2 == 1) return 2800;
}

if (u2 == 4) return 2700;
if (u2 == 3) return 2500;
if (u2 == 2) return 2000;
if (u2 == 1) return 1000;
//if(coveredME >= 1) return -3050;
return 0;
};

//Retourne true ou false s'il y a un tableau combinaison est trouvé
var findArray = function(arr, inArr) {
var fCount = arr.length;
var sCount = inArr.length;
var k;
for (var i = 0; i <= fCount - sCount; i++) {
  k = 0;
  for (var j = 0; j < sCount; j++) {
    if (arr[i + j] == inArr[j]) k++;
    else break;
  }
  if (k == sCount) return true;
}
return false;
};

//Retourne true si il trouve une combinaison donnee dans les tableaux du debut sinon false
var isAnyInArrays = function(combos, arr) {
for (var i = 0; i < combos.length; i++) {
  if (findArray(arr, combos[i])) return true;
}
return false;
};


var winValue = 1000000000;
var valuePosition = function(arr1, arr2, arr3, arr4) { // 4 directions
var w = 0,
  u2 = 0,
  u3 = 0,
  u4 = 0,
  c3 = 0,
  c4 = 0,
  cme = 0,
  cyou = 0;
var allArr = [arr1, arr2, arr3, arr4];
for (var i = 0; i < allArr.length; i++) {
//Si on trouve une position win
  if (isAnyInArrays(win, allArr[i])) {
    w++;
    continue;
  }
 //Si on trouve une position 4 pions avec un pion ennemie sur un cote
  if (isAnyInArrays(covered4, allArr[i])) {
    c4++;
    continue;
  }
  //Si on trouve une position 3 pions avec un pion ennemie sur un cote
  if (isAnyInArrays(covered3, allArr[i])) {
    c3++;
    continue;
  }
  //Si on trouve une position 4 pions sans pion sur les cotes
  if (isAnyInArrays(unCovered4, allArr[i])) {
    u4++;
    continue;
  }
  //Si on trouve une position 3 pions sans pion sur les cotes
  if (isAnyInArrays(unCovered3, allArr[i])) {
    u3++;
    continue;
  }
  //Si on trouve une position 2 pions sans pion sur les cotes
  if (isAnyInArrays(unCovered2, allArr[i])) {
    u2++;
  }
   if (isAnyInArrays(coveredYOU, allArr[i])) {
    cyou++;
  }
  if (isAnyInArrays(coveredME, allArr[i])) {
    cme++;
  }
}
//On retourne le nombre combo trouvé
return valueCombo(w, u2, u3, u4, c3, c4, cme, cyou);
};





//La fonction doit prendre en paramètre le numéro du joueur renvoyer par l'api
//var player = function(player) {

// var combinations = initCombinations();


// Permet juste de vérifier si on a un gagnant sur plateau
// var checkWin = function() {
//  //Parcours du plateau X,Y (I,J)
// for (var i = 0; i < cellsCount; i++) {
//   for (var j = 0; j < cellsCount; j++) {
//    //Si c'est vide on continue
//     if (curState[i][j] == 0) continue;
//     //
//     var playerVal = combinations.valuePosition(
//      //Je créé des tableau dans les 4 directions de longueur 10 à partir de la position courante 
//       getCombo(curState, curState[i][j], i, j, 1, 0),
//       getCombo(curState, curState[i][j], i, j, 0, 1),
//       getCombo(curState, curState[i][j], i, j, 1, 1),
//       getCombo(curState, curState[i][j], i, j, 1, -1)
//     );
//     //Si on a la combinaison gagnante alors on renvoie win = true
//     if (playerVal === winValue) {
//       win = true;
//     }
//   }
// }
//};

//Parametre : player : numéro du joueur, node : une possibilité, depth = 0, parent=board actuelle
var miniMax = function minimax(node, depth, player, parent) {
  //si on a depth a 0 on retourne le resultat de la fonction heuristique
if (depth == 0) return heuristic(node, parent);
//alpha = - infini
// var alpha = -1;
// //Recupere la tableau des coup possible
// var childs = getChilds(node, player);
// //on parcours le tableau des coups possibles
// for (var i = 0; i < childs.length; i++) {
//   //Pour chaque coup possibles on détermine un alpha
//   alpha = Math.max(alpha, -minimax(childs[i], depth - 1, -player, node));
// }
// return alpha;
};

//Fonction qui regarde si la position possible est satisfaisante
var isAllSatisfy = function(candidates, pointX, pointY) {
var counter = 0;
//On parcours le tableau de candidat
for (var i = 0; i < candidates.length; i++) {
  //Si les points sont différents de ce qu'on a dans notre tableau de candidat on augmente le compteur
  if (pointX != candidates[i][0] || pointY != candidates[i][1]) 
    counter++;
}
//renvoie true s'il y a pas l'ensemble de point dans le tableau
return counter == candidates.length;
};

//Tableau de coup possible
var getChilds = function(parent, player) {
  //tableau enfant
var children = [];
//tableau candidat
var candidates = [];
//Pour X de 0 a 18
for (var i = 0; i < cellsCount; i++) {
  //Pour Y de 0 a 18
  for (var j = 0; j < cellsCount; j++) {
    //Si la cellule n'est pas vide
    if (parent[i][j] != 0) {
      //Regarde en X s'il y a un autre pion a une case a coté
      for (var k = i - ring; k <= i + ring; k++) {
        //Regarde en Y s'il y a un autre pion a une case a coté
        for (var l = j - ring; l <= j + ring; l++) {
          //Si X ou Y à l'intérieur du plateau
          if (k >= 0 && l >= 0 && k < cellsCount && l < cellsCount) {
            //Si on trouve une case vide
            if (parent[k][l] == 0) {
              //Point possible ou on peut positionner notre pion
              var curPoint = [k, l];
              var flag = isAllSatisfy(candidates, curPoint[0], curPoint[1]);
              //Si l'ensemble de point X,Y n'est pas déjà dans les candidats on l'ajoute
              if (flag) candidates.push(curPoint);
            }
          }
        }
      }
    }
  }
}
//On parcours le tableau des candidats
for (var f = 0; f < candidates.length; f++) {
  //board temporaire
  var tmp = Array.matrix(cellsCount, cellsCount, 0);
  //Pour X de 0 à 18
  for (var m = 0; m < cellsCount; m++) {
    //Pour Y de 0 à 18
    for (var n = 0; n < cellsCount; n++) {
      //recopie la board dans son état courant
      tmp[m][n] = parent[m][n];

    }
  }
  // toute

  tmp[candidates[f][0]][candidates[f][1]] = player;
  //On ajoute le coup dans un tableau d'enfant
  children.push(tmp);
}
return children;
};

//Check les combos
var getCombo = function(node, curPlayer, i, j, dx, dy) {
  //
var combo = [curPlayer];
//Pour m de 1 a 5 
for (var m = 1; m < gameSize; m++) {
  //i = X, j = Y : on check si on a 5 pions d'affiler dans toutes les directions
  var nextX1 = i - dx * m;
  var nextY1 = j - dy * m;
  // Si on est plus dans le plateau  on casse la boucle
  if (nextX1 >= cellsCount || nextY1 >= cellsCount || nextX1 < 0 || nextY1 < 0) break;
  //Piece a coté
  var next1 = node[nextX1][nextY1];
  //Si la piece a coté est une piece ennemie
  if (node[nextX1][nextY1] == -curPlayer) {
    //On ajoute au début du tableau la position de la pièce 
    combo.unshift(next1);
    break;
  }

  combo.unshift(next1);
}
//pour k de 1 a 5
for (var k = 1; k < gameSize; k++) {
  //i = X, j = Y : on check si on a 5 pions d'affiler dans toutes les directions
  var nextX = i + dx * k;
  var nextY = j + dy * k;
  // Si on est plus dans le plateau  on casse la boucle
  if (nextX >= cellsCount || nextY >= cellsCount || nextX < 0 || nextY < 0) break;
  //Piece a coté
  var next = node[nextX][nextY];
  //Si la piece a coté est une piece ennemie
  if (next == -curPlayer) {
    //On ajoute a la fin du tableau la position de la pièce 
    combo.push(next);
    break;
  }
  combo.push(next);
}
return combo;
};

//fonction d'heuristic (newNode : une possibilité, oldNode=board actuelle)
var heuristic = function(newNode, oldNode) {
  //X de 0 à 18
for (var i = 0; i < cellsCount; i++) {
  //Y de 0 à 18
  for (var j = 0; j < cellsCount; j++) {
    //Si on a une possibilité
    if (newNode[i][j] != oldNode[i][j]) {
      //curCell = la possibilite
      var curCell = newNode[i][j];
      //On regarde autour de la possibilité si c'est nous qui jouons et nous donne le score associé
      var playerVal = valuePosition(
        getCombo(newNode, curCell, i, j, 1, 0),
        getCombo(newNode, curCell, i, j, 0, 1),
        getCombo(newNode, curCell, i, j, 1, 1),
        getCombo(newNode, curCell, i, j, 1, -1)
      );
      //On regarde la possibilité si l'ennemie joue et nous donne le score associé
      newNode[i][j] = -curCell;
      var oppositeVal = valuePosition(
        getCombo(newNode, -curCell, i, j, 1, 0),
        getCombo(newNode, -curCell, i, j, 0, 1),
        getCombo(newNode, -curCell, i, j, 1, 1),
        getCombo(newNode, -curCell, i, j, 1, -1)
      );
      newNode[i][j] = -curCell;
      //Retourne le score
      return 2 * playerVal + oppositeVal;
    }
  }
}
return 0;
};

var main = function(x,y) {
//if (maxPlayer === -1) curState[9][9] = 1;

curState[x][y] = -maxPlayer;
//initialisation du prochain coup a [-1; -1]
var answ = [-1, -1];
var c = getChilds(curState, maxPlayer);

var maxChild = -1;
//maxValue = -infini (plus petite valeur JS)
var maxValue = Number.NEGATIVE_INFINITY;
//Parcours du tableau des positions possibles
for (var k = 0; k < c.length; k++) {
  //Pour chaque possibilité curValue est le score obtenu pour cette possibilité
  var curValue = miniMax(c[k], 0, -maxPlayer, curState);
  
  if (complexity > 1) {
    //var curValue2 = miniMax(c[k], complexity - 1, -maxPlayer, curState);
    //use it for more complex game!
  }
  //On vient prendre le score maximum entre les possibilités
  if (maxValue < curValue) {
    maxValue = curValue;
    maxChild = k;
  }


}
//X de 0 à 18

if(c[maxChild]!=undefined){
for (var i = 0; i < cellsCount; i++) {
  //Pour Y de 0 à 18
  for (var j = 0; j < cellsCount; j++) {

    //Si on a bien une possibilité 
    if (c[maxChild][i][j] != curState[i][j]) {

      //On instancie notre pion
      answ[0] = i;
      answ[1] = j;
      curState[answ[0]][answ[1]]= 1
      //instancie la board avec le nouveau pion placé
      //retourne la position
      answ = '{ "x":'+answ[0]+',"y":'+answ[1]+'}'
      return answ;
    }
  }
}
}
return answ;
};
